from itertools import permutations as perm
sock_index = {'blue':8,'green':6,'black':12,'white':15}
socks = []
sum = 0
for color in sock_index:
    sum+=sock_index[color]
    for i in range(sock_index[color]):
        socks.append(color)
print("There are %i total socks"%sum)

for i in range(2,sum+1):
    found_pair = True
    for pick in perm(socks,i):
        #pick = pick[:i]
        any_paired = False
        for color in sock_index:
            paired = pick.count(color)>1
            if paired:
                any_paired = True
                break
        found_pair = any_paired
        if not found_pair:
            print("When picking %i socks in order %s, a color pair was not found"%(i,pick))
            break
    if found_pair:
        print("Minimum %i"%i)
