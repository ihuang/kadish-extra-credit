import random
import time
var = list(set('sendmoremoney'))
def num(word,m):
  for v in m:
    word = word.replace(v,str(m[v]))
  return int(word)
digits = [0,1,2,3,4,5,6,7,8,9]

d = digits[:]
print ("Solutions:\n")
sols = 0
def foo(s,d,n=0,m={}):
    global sols
    if n==len(s):
        send = num('send',m)
        more = num('more',m)
        money = num('money',m)
        if send+more==money:
            print(m)
            print("%i + %i = %i"%(send,more,money))
            sols+=1
            #raise SystemExit
        return
        #raise SystemExit
    for i in d:
        _d = d[:]
        _d.remove(i)
        char = s[n]
        m[char] = i
        foo(s,_d,n+1,dict(m))
s = time.time()
foo(var,digits)
elapsed = time.time()-s
print("Found %i solutions in %f seconds"%(sols,elapsed))
